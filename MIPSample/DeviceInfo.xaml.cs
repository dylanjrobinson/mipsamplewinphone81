﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Phone.UI.Input;
using mipsample.Models;
using mipsample.ViewModels;
using System.Diagnostics;
using Windows.UI.Xaml.Controls.Primitives;

namespace mipsample
{
    public sealed partial class DeviceInfo : Page
    {
        public BEServiceVM ServiceVMX { get; private set; }
        public BEDeviceModel DeviceM { get; private set; }

        public DeviceInfo()
        {
            //DeviceM = DeviceController.SelectedDevice;

            this.InitializeComponent();

            this.Loaded += (sender, e) => { HardwareButtons.BackPressed += OnBackPressed; };
            this.Unloaded += (sender, e) => { HardwareButtons.BackPressed -= OnBackPressed; };
        }

        private void OnBackPressed(object sender, BackPressedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
                e.Handled = true;
            }
        }

        public void Initialize(BEDeviceModel deviceModel)
        {
            DeviceM = deviceModel;
        }

        public string Name
        {
            get
            {
                return DeviceM.Name.Trim();
            }
        }

        public UInt64 BluetoothAddress
        {
            get
            {
                return DeviceM.BluetoothAddress;
            }
        }

        public String DeviceId
        {
            get
            {
                return DeviceM.DeviceId;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //DeviceM = DeviceController.SelectedDevice;

            //Get Send Data Service
            GlobalSettings.SelectedService = GlobalSettings.SelectedDevice.ServiceModels.FirstOrDefault(p => p.Uuid.ToString().ToLower().Contains("ffe5"));
            GlobalSettings.SelectedService.InitializeCharacteristics();

            //Get the particular endpoint of that service
            GlobalSettings.SelectedCharacteristic = GlobalSettings.SelectedService.CharacteristicModels.FirstOrDefault(p => p.Uuid.ToString().ToLower().Contains("ffe9"));
        }

        private void SendCommand(string command)
        {
            Utilities.RunFuncAsTask(GlobalSettings.SelectedCharacteristic.ReadValueAsync);
            Utilities.RunFuncAsTask(async () => await GlobalSettings.SelectedCharacteristic.WriteMessageAsync(command));
        }

        private void Cmd_Sound1_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("playsound_1");
        }

        private void Cmd_BlueChest_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("changechestcolorblue");
        }

        private void Cmd_RedChest_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("changechestcolorred");
        }

        private void Cmd_GreenChest_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("changechestcolorgreen");
        }

        private void Cmd_Offback_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("getupback");
        }

        private void Cmd_MP3_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("mp3");
        }

        private void Cmd_TurnLeft_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("left");
        }

        private void Cmd_TurnRight_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("right");
        }

        private void Cmd_DriveForward_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("forward");
        }

        private void Cmd_DriveBack_Click(object sender, RoutedEventArgs e)
        {
            SendCommand("back");
        }

        //Sliders
        private void Slider_UpDown_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var slider = sender as Slider;
            if (slider.Value > 50)
            {
                int value = (int)slider.Value - 50;
                SendCommand("forward," + value);
            }

            if (slider.Value < 50)
            {
                int value = 50 - (int)slider.Value;
                SendCommand("back," + value);
            }
        }

        private void Slider_LeftRight_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            var slider = sender as Slider;
            if (slider.Value > 50)
            {
                int value = (int)slider.Value - 50;
                SendCommand("right");
            }

            if (slider.Value < 50)
            {
                int value = 50 - (int)slider.Value;
                SendCommand("left");
            }
        }


    }
}
