﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth; 
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml.Media;
using mipsample.Dictionary;
using mipsample.Models; 
using mipsample.ViewModels;
using Windows.ApplicationModel.Background;
using Windows.UI.Popups;

namespace mipsample
{
    /// <summary>
    /// Global settings for the app.
    /// 
    /// Acts as a singleton for the entire app, containing the list of all devices,
    /// storing details of the currently displayed pages, as well as maintaining our
    /// list of custom service and characteristic names.
    /// 
    /// Several settings and the currently registered gatt triggers are also tracked
    /// here.
    /// </summary>
    public static class GlobalSettings
    {
        #region ----------------------------- Variables --------------------------
        // For navigation through pages
        public static BEDeviceModel SelectedDevice;
        public static BEServiceModel SelectedService;
        public static BECharacteristicModel SelectedCharacteristic; 

        // Dictionaries for keeping track of objects
        public static ServiceDictionary ServiceDictionaryConstant;
        public static ServiceDictionary ServiceDictionaryUnknown;
        public static CharacteristicDictionary CharacteristicDictionaryConstant; 
        public static CharacteristicDictionary CharacteristicDictionaryUnknown;
        public static Dictionary.DataParser.CharacteristicParserLookupTable ParserLookupTable;
        public static bool DictionariesCleared { get; private set; }

        // List of active devices
        public static List<BEDeviceModel> PairedDevices;

        // Toast and background related objects
        public static bool BackgroundAccessRequested;
        public static List<BECharacteristicModel> CharacteristicsWithActiveToast;

        // List of settings
        private const string USE_CACHED_MODE = "UseCachedMode";
        private static bool _useCachedMode = false;
        
        public static bool UseCachedMode
        {
            get
            {
                return _useCachedMode;
            } 
            set
            {
                SetUseCachedMode(value);
            }
        }
        #endregion // variables

        /// <summary>
        /// Initializes the GlobalSettings class.  Also reads the stored cache mode setting.
        /// </summary>
        public static void Initialize()
        {
            // Create objects for the objects that we need. 
            PairedDevices = new List<BEDeviceModel>();

            ParserLookupTable = new Dictionary.DataParser.CharacteristicParserLookupTable();
            ServiceDictionaryUnknown = new ServiceDictionary();
            ServiceDictionaryConstant = new ServiceDictionary();
            CharacteristicDictionaryUnknown = new CharacteristicDictionary();
            CharacteristicDictionaryConstant = new CharacteristicDictionary();
            ServiceDictionaryConstant.InitAsConstant();
            CharacteristicDictionaryConstant.InitAsConstant();
            DictionariesCleared = false;
            CharacteristicsWithActiveToast = new List<BECharacteristicModel>();

            _useCachedMode = ApplicationData.Current.LocalSettings.Values.ContainsKey(USE_CACHED_MODE);
        }

        /// <summary>
        /// Async initialization function that loads our characteristic dictionaries
        /// </summary>
        /// <returns></returns>
        public static async Task InitializeDictionariesAsync()
        {
            await ServiceDictionaryUnknown.LoadDictionaryAsync();
            await CharacteristicDictionaryUnknown.LoadDictionaryAsync();
        }


        public static async Task PopulateDeviceListAsync()
        {
            // Remove all devices and start from scratch
            PairedDevices.Clear();

            // Asynchronously get all paired/connected bluetooth devices. 
            var infoCollection = await DeviceInformation.FindAllAsync(BluetoothLEDevice.GetDeviceSelector());

            // Re-add devices
            foreach (DeviceInformation info in infoCollection)
            {
                // Make sure we don't initialize duplicates
                if (PairedDevices.FindIndex(device => device.DeviceId == info.Id) >= 0)
                {
                    continue;
                }
                BluetoothLEDevice WRTDevice = await BluetoothLEDevice.FromIdAsync(info.Id);
                BEDeviceModel deviceM = new BEDeviceModel();
                deviceM.Initialize(WRTDevice, info);

                //attach only Mip
                if(deviceM.Name.ToLower().Contains("mip"))
                {
                    PairedDevices.Add(deviceM);
                }
            }
        }

        #region ----------------------------- Notifications -------------------------
        /// <summary>
        /// Unregisters all notifications by all characteristics on all paired devices.
        /// Saves battery on the devices.
        /// </summary>
        /// <returns></returns>
        public static async Task UnregisterAllNotificationsAsync()
        {
            try
            {
                foreach (var deviceModel in PairedDevices)
                {
                    await deviceModel.UnregisterNotificationsAsync();
                }
            }
            catch (Exception ex)
            {
                Utilities.OnExceptionWithMessage(ex, "Hit an exception unregistering all notifications.");
            }
        }

        /// <summary>
        /// Registers for all notifications by all characteristics on all paired devices.
        /// Allows us to receive characteristic value updates, and acts as a secondary
        /// mechanism to inform the OS to auto-connect to the paired devices if they begin
        /// advertising.
        /// </summary>
        /// <returns></returns>
        public static async Task RegisterAllNotificationsAsync()
        {
            try
            {
                foreach (var deviceModel in PairedDevices)
                {
                    await deviceModel.RegisterNotificationsAsync();
                }
            }
            catch (Exception ex)
            {
                Utilities.OnExceptionWithMessage(ex, "Hit an exception Registering for all notifications.");
            }
        }
        #endregion // Notifications

        #region ----------------------------- Toasts and Background -------------------------
        /// <summary>
        /// Request background access so that we can register toasts.
        /// Do this once to begin with so that we don't have to do this everywhere.
        /// </summary>
        /// <returns></returns>
        public static async Task RequestBackgroundAccessAsync()
        {
            if (BackgroundAccessRequested)
            {
                return;
            }
            
            // Request access to run toast in background
            var result = await BackgroundExecutionManager.RequestAccessAsync();

            // Check if got access
            if (result == BackgroundAccessStatus.Denied)
            {
                BackgroundAccessRequested = false; 
            }
            else
            {
                BackgroundAccessRequested = true;
            }
        }
        #endregion

        #region ----------------------------- Dictionary cleanup -------------------------
        /// <summary>
        /// Clear out all custom names in the dictionaries.
        /// </summary>
        /// <returns></returns>
        public static async Task ClearDictionariesAsync()
        {
            await ServiceDictionaryUnknown.ClearDictionaryAsync();
            await CharacteristicDictionaryUnknown.ClearDictionaryAsync();
            DictionariesCleared = true;
        }
        #endregion // Dictionary cleanup

        #region ----------------------------- Other settings -------------------------
        /// <summary>
        /// Sets and persists whether to use cached mode or not
        /// </summary>
        /// <param name="value"></param>
        private static void SetUseCachedMode(bool value)
        {
            if (value)
            {
                ApplicationData.Current.LocalSettings.Values[USE_CACHED_MODE] = "1";
            }
            else
            {
                ApplicationData.Current.LocalSettings.Values.Remove(USE_CACHED_MODE);
            }
            _useCachedMode = value;
        }
        #endregion // Other settings
    }
}
