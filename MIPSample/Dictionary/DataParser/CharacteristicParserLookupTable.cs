﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Storage.Streams;

using TIUuids = mipsample.Extras.TI_BLESensorTagGattUuids.TISensorTagCharacteristicUUIDs;
using TIParsers = mipsample.Extras.TI_BLESensorTagCharacteristicParsers;

namespace mipsample.Dictionary.DataParser
{
    public class CharacteristicParserLookupTable : Dictionary<Guid, Func<IBuffer, string>>
    {
        // Contains the default string parsers for the characteristics. 
        public CharacteristicParserLookupTable()
        {
            /* WinRT implemented BTLE specification characteristics */
            this.Add(GattCharacteristicUuids.AlertCategoryId, BLE_Specification.AlertCategoryId.ParseBuffer);
            this.Add(GattCharacteristicUuids.AlertCategoryIdBitMask, BLE_Specification.AlertCategoryIdBitMask.ParseBuffer);
            this.Add(GattCharacteristicUuids.AlertLevel, BLE_Specification.AlertLevel.ParseBuffer);
            this.Add(GattCharacteristicUuids.AlertNotificationControlPoint, BLE_Specification.AlertNotificationControlPoint.ParseBuffer);
            this.Add(GattCharacteristicUuids.AlertStatus, BLE_Specification.AlertStatus.ParseBuffer);
            this.Add(GattCharacteristicUuids.BatteryLevel, BasicParsers.ParseUInt8);
            this.Add(GattCharacteristicUuids.BootKeyboardInputReport, BasicParsers.ParseUInt8Multi);
            this.Add(GattCharacteristicUuids.BootKeyboardOutputReport, BasicParsers.ParseUInt8Multi);
            this.Add(GattCharacteristicUuids.BootMouseInputReport, BasicParsers.ParseUInt8Multi);
            this.Add(GattCharacteristicUuids.CscFeature, BLE_Specification.CscFeature.ParseBuffer);
            this.Add(GattCharacteristicUuids.DayOfWeek, BLE_Specification.DayOfWeek.ParseBuffer);
            this.Add(GattCharacteristicUuids.GapDeviceName, BasicParsers.ParseString);
            this.Add(GattCharacteristicUuids.HardwareRevisionString, BasicParsers.ParseString);
            this.Add(GattCharacteristicUuids.ManufacturerNameString, BasicParsers.ParseString);
            this.Add(GattCharacteristicUuids.ModelNumberString, BasicParsers.ParseString);
            this.Add(GattCharacteristicUuids.SerialNumberString, BasicParsers.ParseString);
            this.Add(GattCharacteristicUuids.SoftwareRevisionString, BasicParsers.ParseString);
            this.Add(GattCharacteristicUuids.FirmwareRevisionString, BasicParsers.ParseString);
        }
    }
}
