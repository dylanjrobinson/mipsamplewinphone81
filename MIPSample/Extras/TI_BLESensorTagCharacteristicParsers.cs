﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;

namespace mipsample.Extras
{
    /// <summary>
    /// Parsers used for characteristics exposed by the TI SensorTag.
    /// 
    /// ATTRIBUTION
    /// "TI SensorTag User Guide" by Texas Instruments, used under CC BY-SA 3.0 US / Desaturated from original
    /// http://processors.wiki.ti.com/index.php/SensorTag_User_Guide
    /// </summary>
    class TI_BLESensorTagCharacteristicParsers
    {
        /// <summary>
        /// Reads a 16-bit signed integer
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static long readInt16TI(DataReader reader)
        {
            uint ObjLSB = reader.ReadByte();
            int ObjMSB = (int)((sbyte)reader.ReadByte());

            long result = (ObjMSB << 8) + ObjLSB;
            return result; 
        }

        /// <summary>
        /// Reads a 16-bit unsigned integer
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static long readUint16TI(DataReader reader)
        {
            uint ObjLSB = reader.ReadByte();
            int ObjMSB = (int)reader.ReadByte();

            long result = (ObjMSB << 8) + ObjLSB;
            return result;
        }
    }
}
